from django.shortcuts import render, HttpResponseRedirect, redirect, get_object_or_404
from django.http import JsonResponse, Http404, HttpResponse
from django.urls import resolve, Resolver404, reverse
from django.utils import translation
from django.contrib import messages
from django.views.generic import TemplateView
from django.core.exceptions import PermissionDenied
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth import get_user_model

from django.contrib.auth import authenticate,login,logout
from django.utils.translation import activate, get_language_info
from django.conf import settings
from django.utils.translation import ugettext as _
from .forms import OrganizationForm, LoginForm, ForgotAccountForm, GeryeJwaForm

from .models import Organization, User
from portal.models import Partner

import hashlib
import string
import random
import datetime

# Create your views here.


def index(request):
	context = {
		'active_item':"index",
	}
	return render(request,'index/index.html',context)


def signin(request):
	if request.user.is_authenticated:
		return redirect('index')
	context = {}
	url = request.GET.get("next")
	form = LoginForm(request.POST or None)
	if request.method == 'POST':
		if form.is_valid():
			email = request.POST.get("email","").lower().strip()
			password = request.POST.get("password")

			user = authenticate(request,email=email,password=password)

			if user and hasattr(user,'profile'):
				login(request,user)
				if 'next' in request.session:
					return HttpResponseRedirect(request.session.pop("next"))
				return redirect("index")
			else:
				context['email'] = email
				context['erreur'] = True
				messages.add_message(request,messages.WARNING,_("Unknown user: Email or password is incorrect!"))
		else:
			messages.add_message(request,messages.WARNING,_("Unknown user: Email or password is incorrect!"))

	context['form'] = form
	return render(request,"index/signin.html",context)

# @access_guarentee(login_is_required=True,check_confirm=False)
def signout(request):
	if request.is_ajax() and request.method == "POST":
		
		print(request.session.items())
		if 'next' in request.session:
			url = request.session.pop("next")
			logout(request)
			return JsonResponse({"url":url})
		logout(request)
		return JsonResponse({"url":reverse("signin")})
	raise PermissionDenied

def set_language(request):
	"""
	Redirect to a given URL while setting the chosen language in the session
	(if enabled) and in a cookie. The URL and the language code need to be
	specified in the request parameters.

	Since this view changes how the user will see the rest of the site, it must
	only be accessed as a POST request. If called as a GET request, it will
	redirect to the page in the request (the 'next' parameter) without changing
	any state.
	"""
	from django.utils.http import is_safe_url
	LANGUAGE_QUERY_PARAMETER = 'lg'
	from django.utils.translation import (
	    LANGUAGE_SESSION_KEY, check_for_language, get_language,
	)
	from django.urls import translate_url
	next = request.GET.get('next', request.GET.get('next'))
	if ((next or not request.is_ajax()) and
			not is_safe_url(url=next, allowed_hosts={request.get_host()}, require_https=request.is_secure())):
		next = request.META.get('HTTP_REFERER')
		next = next and unquote(next)  # HTTP_REFERER may be encoded.
		if not is_safe_url(url=next, allowed_hosts={request.get_host()}, require_https=request.is_secure()):
			next = '/'
	response = HttpResponseRedirect(next) if next else HttpResponse(status=204)
	# if request.method == 'POST':
	lang_code = request.GET.get(LANGUAGE_QUERY_PARAMETER)
	if lang_code and check_for_language(lang_code):
		if next:
			next_trans = translate_url(next, lang_code)
			if next_trans != next:
				response = HttpResponseRedirect(next_trans)
		if hasattr(request, 'session'):
			request.session[LANGUAGE_SESSION_KEY] = lang_code
		response.set_cookie(
			settings.LANGUAGE_COOKIE_NAME, lang_code,
			max_age=settings.LANGUAGE_COOKIE_AGE,
			path=settings.LANGUAGE_COOKIE_PATH,
			domain=settings.LANGUAGE_COOKIE_DOMAIN,
		)
		# Custom code to record the language of user
		if request.user.is_authenticated:
			up = request.user.profile
			up.language_set.filter(is_active=True).update(is_active=False,date_change_active=NOW())
			Language.objects.create(
				raw = lang_code,
				is_active = True,
				date_change_active = NOW(),
				profile = up,
			)
	return response

# @access_guarentee(login_is_required=False,check_confirm=False)
def accountConfirmation(request,activation_key):
	up = get_object_or_404(Profile,activation_key=activation_key,user__isnull=False)
	
	if not up.is_active:
		logout(request)
		login(request,up.user)
		return redirect("accountDisabled")

	if not up.is_account_confirmed or not up.active_email.is_confirmed:
		if NOW() >= up.date_key_expired:
			messages.add_message(request,messages.WARNING,_("Your confirmation code has expired since %s") 
				% (up.date_key_expired.strftime("%B {S}, %Y at %I:%M:%S %p").replace("{S}",str(up.date_key_expired.day) + suffix(up.date_key_expired.day))))
			logout(request)
			login(request,up.user)
			return redirect("accountConfirmationExpired")

		
		now = NOW()
		if not up.is_account_confirmed:
			up.is_account_confirmed = True
			up.date_account_confirmed = now

		ae = up.active_email
		ae.is_confirmed = True
		ae.date_confirmed = now
		ae.save()

		up.activation_key = ""
		up.date_key_expired = None
		up.nb_send_email = 0
		up.save()

		logout(request)
		login(request,up.user)
		
		messages.add_message(request,messages.SUCCESS,_("Perfect %s, Your email account has been successfully confirmed") % up.full_name)

	else:
		messages.add_message(request,messages.SUCCESS,_("This email account is already confirmed. Just log in with your email and password."))
	return redirect("index")

# @access_guarentee(login_is_required=True,check_confirm=False)
def accountConfirmationExpired(request):
	up = request.user.profile
	if up.is_account_confirmed:
		return redirect("index")
	return render(request,"index/account-confirmation-expired.html")

# @access_guarentee(login_is_required=False,check_confirm=False)
def forgotAccount(request):
	if request.user.is_authenticated:
		return redirect('index')
	form = ForgotAccountForm(request.POST or None)
	if request.method == 'POST':
		if form.is_valid():
			email = form.cleaned_data.get('email')
			user = get_user_model().objects.get(email=email)
			up = user.profile
			if not up.forgot_account:
				salt = hashlib.sha1(str(random.random()).encode()).hexdigest()
				up.password_token = hashlib.sha1((salt).encode()).hexdigest()[:30]
				up.date_password_token_expired = NOW() + datetime.timedelta(hours=DEADLINE_PASSWORD_TOKEN_TO_EXPIRE)
				up.save()
				# up.all_forgot_account_clicks.filter(is_active=True).update(is_active=False)
				fa = ForgotAccount.objects.create(
					password = up.default_password,
					is_active = True,
					date_change_active = NOW(),
					profile = up,
				)

			messages.add_message(request,messages.SUCCESS,_("We have sent steps to reset your passsowrd to %s") % up.email)

			answer = send_courriel(host_name=request.META['HTTP_HOST'],instance=up.user,template='reset-password')
			# up.is_password_changed = False
			# up.save()

	context = {
		'form':form,
	}
	return render(request,'index/forgot-account.html',context)

# @access_guarentee(login_is_required=False,check_confirm=False)
def passwordTokenConfirmation(request,password_token):
	up = get_object_or_404(Profile,password_token=password_token)
	if not up.forgot_account:
		raise Http404
	
	if not up.is_active:
		logout(request)
		login(request,up.user)
		return redirect("accountDisabled")

	if up.forgot_account:
		if NOW() >= up.date_password_token_expired:
			messages.add_message(request,messages.WARNING,_("Your confirmation code has expired since %s") 
				% (up.date_password_token_expired.strftime("%B {S}, %Y at %I:%M:%S %p").replace("{S}",str(up.date_password_token_expired.day) + suffix(up.date_password_token_expired.day))))
			logout(request)
			login(request,up.user)
			return redirect("passwordTokenConfirmationExpired")

		fa = up.forgot_account
		fa.is_confirmed = True
		fa.date_confirmed = NOW()
		fa.save()

		logout(request)
		login(request,up.user)
		
		messages.add_message(request,messages.SUCCESS,_("Perfect %(name)s, Please provide your new password") % {"name":up.full_name})

		return redirect("resetPassword")
	else:
		messages.add_message(request,messages.SUCCESS,_("These steps are no longer valid"))
	return redirect("index")

# @access_guarentee(login_is_required=True,check_confirm=False)
def resetPassword(request):
	user = request.user 
	up = user.profile

	# j = up.passwords.all()[0]
	# j.is_active = True 
	# j.save()
	
	form = ResetPasswordForm(data=request.POST or None,user=user)
	if request.method == 'POST':
		if form.is_valid():
			new_password = form.cleaned_data.get('new_password')

			dp = up.default_password
			dp.is_active = False 
			dp.date_change_active = NOW()
			

			p = Password.objects.create(
				raw = new_password,
				is_active = True,
				date_change_active = NOW(),
				profile = up,
			)
			dp.save()

			up.all_forgot_account_clicks.filter(is_active=True).update(is_active=False,date_change_active = NOW())
			up.password_token = ""
			up.date_password_token_expired = None
			
			user.set_password(new_password)
			user.save()
			# update_session_auth_hash(request, user)

			messages.add_message(request,messages.SUCCESS,_("Password reset successfully. Please login with your new password"))

			return redirect("index")
	context = {'form':form}
	return render(request,"index/reset-password.html",context)

# @access_guarentee(login_is_required=True,check_confirm=False)
def passwordTokenConfirmationExpired(request):
	up = request.user.profile
	if up.is_account_confirmed:
		return redirect("index")
	return render(request,"index/account-confirmation-expired.html")

# @access_guarentee(login_is_required=True,check_confirm=False)
def sendEmail(request):
	up = request.user.profile
	url = request.GET.get('next')
	if not up.is_account_confirmed or not up.active_email.is_confirmed:

		# Has already send email and has a ban
		if up.email_sent and isinstance(up.date_email_sent,datetime.datetime):
			deadline_ban = up.date_email_sent + datetime.timedelta(minutes=3)
			if deadline_ban < NOW():
				up.nb_send_email = 0
				up.save()
		if up.nb_send_email < TOTAL_SEND_EMAIL:
			# TO detect wheter it's a new email or just when the user creates the account
			if up.is_account_confirmed and up.email_set.count() > 1:
				answer = send_courriel(host_name=request.META['HTTP_HOST'],instance=up.user,template='confirmation-mail-2')
			else:
				answer = send_courriel(host_name=request.META['HTTP_HOST'],instance=up.user)

			if answer:
				up.email_sent = True 
				up.date_email_sent = NOW()
			
				if NOW() >= up.date_key_expired:
					salt = hashlib.sha1(str(random.random()).encode()).hexdigest()
					up.activation_key = hashlib.sha1((salt+up.email).encode()).hexdigest()
					up.date_key_expired = NOW() + datetime.timedelta(days=DEADLINE_ACTIVATION_KEY_TO_EXPIRE)
				up.nb_send_email += 1
				up.save()
				if up.nb_send_email == TOTAL_SEND_EMAIL:
					messages.add_message(request,messages.INFO,_("Confirmation mail sent to %(email)s, you are no longer able to send mail before 1 hour") % {'email':up.email})
				elif up.nb_send_email == (TOTAL_SEND_EMAIL - 1):
					messages.add_message(request,messages.SUCCESS,_("Confirmation mail sent to %(email)s, you have %(tries)s try left") % {'email':up.email,'tries':(TOTAL_SEND_EMAIL - up.nb_send_email)})
				else:
					messages.add_message(request,messages.SUCCESS,_("Confirmation mail sent to %(email)s, you have %(tries)s tries left") % {'email':up.email,'tries':(TOTAL_SEND_EMAIL - up.nb_send_email)})
			else:
				messages.add_message(request,messages.ERROR,_("ERROR: Confirmation mail did not send to %(email)s") % {"email":up.user.email},extra_tags='danger')
		else:
			future = local_time(up.date_email_sent + datetime.timedelta(minutes=3))
			messages.add_message(request,messages.ERROR,_("You can not send confirmation mail anymore before %(t)s") % {"t":(
				future.strftime("%B {S}, %Y at %I:%M:%S %p").replace("{S}",str(future.day) + suffix(future.day)))},extra_tags='danger')
	else:
		messages.add_message(request,messages.INFO,_("This account is already confirmed"))
	try:
		resolve(url)
		return redirect(url)
	except Resolver404:
		return redirect("index")





def about(request):
	context = {
		'active_item':"about",
	}
	return render(request,'index/about.html',context)

def story(request):
	context = {
		'active_item':"about",
	}
	return render(request,'index/story.html',context)

def team(request):
	context = {
		'active_item':"about",
	}
	return render(request,'index/team.html',context)

def program(request):
	context = {
		'active_item':"program",
	}
	return render(request,'index/program.html',context)

def becomeGeryeJwa(request):
	form = GeryeJwaForm(request.POST or None)
	context = {
		'form':form,
	}
	return render(request,'index/become-gerye-jwa.html',context)

def volunteer(request):
	form = GeryeJwaForm(request.POST or None)
	context = {
		'form':form,
	}
	return render(request,'index/volunteer.html',context)

def community(request):
	context = {
		'active_item':"community",
	}
	return render(request,'index/community.html',context)

def involve(request):
	context = {
		'active_item':"involve",
	}
	return render(request,'index/involve.html',context)

def donate(request):
	context = {
		'active_item':"donate",
	}
	return render(request,'index/donate.html',context)


def otherDonation(request):
	context = {
		'active_item':"donate",
	}
	return render(request,'index/other-donation.html',context)

def contact(request):
	context = {
		'active_item':"contact",
	}
	return render(request,'index/contact.html',context)

def institutions(request):
	context = {
	
	}
	return render(request,'index/institutions.html',context)

def partners(request):
	all_partners = Partner.objects.filter(active=True).order_by('-id')
	context = {
		'all_partners':all_partners,
	}
	return render(request,'index/partners.html',context)

def partnerForm(request):
	form = OrganizationForm(request.POST or None)
	if request.method == 'POST':
		if form.is_valid():
			kind = form.cleaned_data.get("kind")
			o = form.save(commit=False)
			if kind == '-':
				other_kind = form.cleaned_data.get("other_kind")
				o.other_kind = other_kind
			o.save()

			request.session['token'] = hashlib.sha1(str(random.random()).encode()).hexdigest()
			request.session['id'] = o.id

			return redirect("partnerFormReview")
		else:
			messages.warning(request,'Form invalid')
	context = {
		'form':form,
	}
	return render(request,'index/partner-form.html',context)

def partnerFormReview(request):
	token = request.session.get("token")
	if not token or not request.session:
		return PermissionDenied

	o = Organization.objects.get(id=request.session.get('id'))
		
	context = {
		'o':o,
	}
	return render(request,'index/partner-form-review.html',context)

def gallery(request):
	context = {
	
	}
	return render(request,'index/gallery.html',context)

def map(request):
	context = {
		'map':map,
	}
	return render(request,'index/map.html',context)

def galleryChildren(request):
	context = {
	
	}
	return render(request,'index/gallery-children.html',context)

