from django.db.models.signals import post_save, pre_delete
from django.utils.translation import ugettext_lazy as _
from django.utils.deconstruct import deconstructible
from django.urls import reverse
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.auth.models import (
	AbstractBaseUser,PermissionsMixin
)
from django.dispatch import receiver
from django.conf import settings
from django.urls import reverse
from django.db import models

from django.conf import settings

from geryejwa.utils import (
	NOW,create_code,create_slug,
)

import random
import hashlib
import datetime
import string


# Create your models here.
from django.contrib.auth.models import BaseUserManager

class UserManager(BaseUserManager):
	def _create_user(self, email, password, **extra_fields):
		if not email:
			raise ValueError('The Email must be set')
		email = self.normalize_email(email)
		user = self.model(email=email,date_joined=NOW(), **extra_fields)
		user.set_password(password)
		user.save()
		return user

	def create_user(self,email,password,**extra_fields):
		return self._create_user(email, password, **extra_fields)

	def create_superuser(self, email, password, **extra_fields):
		extra_fields.setdefault('is_staff', True)
		extra_fields.setdefault('is_superuser', True)
		extra_fields.setdefault('is_active', True)

		if extra_fields.get('is_staff') is not True:
			raise ValueError('Superuser must have is_staff=True.')
		if extra_fields.get('is_superuser') is not True:
			raise ValueError('Superuser must have is_superuser=True.')
		return self._create_user(email, password, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
	email = models.EmailField(unique=True, null=True)
	first_name = models.CharField(_('First name'),max_length=255)
	last_name = models.CharField(_('Last name'),max_length=255)
	date_joined = models.DateTimeField(_('date joined'), default=NOW)

	is_staff = models.BooleanField(
		_('staff status'),
			default=False,
			help_text=_('Designates whether the user can log into this site.'),
		)
	is_active = models.BooleanField(
		_('active'),
			default=True,
			help_text=_(
				'Designates whether this user should be treated as active. '
				'Unselect this instead of deleting accounts.'
			),
		 )
	USERNAME_FIELD = 'email'
	objects = UserManager()

	def __str__(self):
		return self.email

	def get_full_name(self):
		return self.email

	def get_short_name(self):
		return self.email


class Profile(models.Model):
	date_created 					= models.DateTimeField(auto_now_add=True)
	date_modified					= models.DateTimeField(auto_now=True)

	slug 							= models.SlugField(max_length = 80)
	code 							= models.CharField(max_length=80,unique=True)

	user 							= models.OneToOneField(settings.AUTH_USER_MODEL,blank=True,null=True,
										on_delete=models.CASCADE)
	
	xphoto 							= models.ForeignKey("Photo",verbose_name="Photo",
										blank=True,null=True,on_delete=models.SET_NULL)

	MALE,FEMALE 					= 'M','F'
	GENDER 							= [(MALE,_('Male')),(FEMALE,_('Female'))]
	gender 							= models.CharField(max_length=2,blank=True,choices=GENDER)

	area_code 						= models.CharField(max_length=7,blank=True)
	phone 							= models.CharField(max_length=22,blank=True)
	
	done 							= models.BooleanField(default=False)
	date_done 						= models.DateTimeField(null=True,blank=True)

	activation_key 					= models.CharField(max_length=255,blank=True)
	date_key_expired 				= models.DateTimeField(blank=True,null=True)
	is_account_confirmed 			= models.BooleanField(default=False)
	date_account_confirmed 			= models.DateTimeField(blank=True,null=True)

	password_token 					= models.CharField(max_length=255,blank=True)
	date_password_token_expired 	= models.DateTimeField(blank=True,null=True)

	is_password_changed 			= models.BooleanField(default=True)
	date_password_changed 			= models.DateTimeField(blank=True,null=True)	

	nb_send_email					= models.IntegerField(default=0)
	email_sent 						= models.BooleanField(default=False)
	date_email_sent 				= models.DateTimeField(blank=True,null=True)

	def __str__(self):
		return self.full_name

	@property 
	def first_name(self):
		return self.user.first_name 

	@property 
	def full_name(self):
		if self.first_name or self.last_name:
			return "%s %s" % (self.first_name,self.last_name)
		return ""

	def get_absolute_url(self):
		return reverse('profile',args=[self.slug])

	@property
	def last_name(self):
		return self.user.last_name

	@property
	def photo_exists(self):
		if self.xphoto:
			return self.xphoto.photo_exists
		return False

	@property
	def email(self):
		return self.user.email

	
	def save(self, *args, **kwargs):
		if self.pk is None:
			self.code = create_code(self,40)	
			self.slug = create_slug(self,field_name='full_name')
		super(Profile, self).save(*args, **kwargs)

@receiver(post_save, sender=User)
def update_profile(sender, instance, created, **kwargs):
	if created:
		salt = hashlib.sha1(str(random.random()).encode()).hexdigest()
		activation_key = hashlib.sha1((salt).encode()).hexdigest()
		date_key_expired = NOW() + datetime.timedelta(hours=48)

		Profile.objects.create(user=instance,activation_key=activation_key,date_key_expired=date_key_expired)
	instance.profile.save()

@deconstructible
class object_directory_path(object):

	def __init__(self, prefix):
		self.prefix = prefix

	def __call__(self, instance, filename):
		ext = filename.split('.')[-1]
		date = NOW().strftime("%Y-%m-%dT%H:%M:%S")
		if instance.pk:
			ID = instance.pk
		else:
			ID = instance.generer(4).upper()

		filename = "uploaded-photos/%s/IMG-%s-ID-%s.%s" % (self.prefix,date,ID, ext)
		return filename

class Photo(models.Model):
	date_created = models.DateTimeField(auto_now_add=True)
	date_modified = models.DateTimeField(auto_now=True)

	code = models.CharField(max_length=11)

	photo = models.ImageField(upload_to=object_directory_path('original_size'), blank=True)
	photo600 = models.ImageField(upload_to=object_directory_path('600x600'), blank=True)
	photo450 = models.ImageField(upload_to=object_directory_path('450x450'), blank=True)
	photo300 = models.ImageField(upload_to=object_directory_path('300x300'), blank=True)
	photo200 = models.ImageField(upload_to=object_directory_path('200x200'), blank=True)
	photo150 = models.ImageField(upload_to=object_directory_path('150x150'), blank=True)
	photo100 = models.ImageField(upload_to=object_directory_path('100x100'), blank=True)
	photo75 = models.ImageField(upload_to=object_directory_path('75x75'), blank=True)
	photo37 = models.ImageField(upload_to=object_directory_path('37x37'), blank=True)
	photo18 = models.ImageField(upload_to=object_directory_path('18x18'), blank=True)

	def delete_all_photos(self):
		self.photo.delete()
		self.photo600.delete()
		self.photo450.delete()
		self.photo300.delete()
		self.photo200.delete()
		self.photo150.delete()
		self.photo100.delete()
		self.photo75.delete()
		self.photo37.delete()
		self.photo18.delete()

	def __str__(self):
		return str(self.id)

	@property
	def get_absolute_image_url(self):
		return self.photo.url

	@property
	def photo_exists(self):
		if self.photo and file_exists(self.photo):
			return True
		return False

	@property
	def width(self):
		return self.photo.width

	def generer(self,nb_letter):
		letters_digits = string.ascii_letters + string.digits
		aleatoire = [random.choice(letters_digits) for _ in range(nb_letter)]
		return ''.join(aleatoire)
		
	@property
	def height(self):
		return self.photo.height

	@property
	def ratio(self):
		if self.width and self.height:
			return self.width / self.height

	def save(self, *args, **kwargs):
		if self.pk is None:
			self.code = create_code(self,11)

		if self.photo:
			if not self.photo600 or not self.photo450 or not self.photo300 or not self.photo200 or self.photo150 \
			or not self.photo75	or not self.photo37 or not self.photo18:
			# SIZES = [(600,600),(450,450),(300,300),(200,200),(150,150),(100,100),(75,75),(37,37),(18,18)]
				p = CompressImage(self.photo)
				self.photo600 = p.resize(size=(600,600),render=True)
				self.photo450 = p.resize(size=(450,450),render=True)
				self.photo300 = p.resize(size=(300,300),render=True)
				self.photo200 = p.resize(size=(200,200),render=True)
				self.photo150 = p.resize(size=(150,150),render=True)
				self.photo100 = p.resize(size=(100,100),render=True)
				self.photo75 = p.resize(size=(75,75),render=True)
				self.photo37 = p.resize(size=(37,37),render=True)
				self.photo18 = p.resize(size=(18,18),render=True)
		super(Photo, self).save(*args, **kwargs)

@receiver(pre_delete, sender=Photo)
def deletePhotoWith(sender, instance, **kwargs):
	instance.delete_all_photos()

class Organization(models.Model):
	name = models.CharField(max_length=255)
	mission = models.TextField(max_length=4000)
	KIND = [
		('S',_('School')),('NS',_('Normal School')),
		('O',_('Orphanage')),('A',_('Association')),
		('NGO',_('Non-Governmental Organization')),
		('F',_('Foundation')),
		('-',_('Other')),
	]
	kind = models.CharField(max_length=3,choices=KIND)
	other_kind = models.CharField(max_length=3,choices=KIND)

	address = models.CharField(max_length=255)
	street_address = models.CharField(max_length=255)
	address_line_2 = models.CharField(max_length=255)
	city = models.CharField(max_length=255)
	state = models.CharField(max_length=255)
	zip_code = models.CharField(max_length=255,verbose_name=_('ZIP / Postal Code'))

	first_name = models.CharField(max_length=255)
	last_name = models.CharField(max_length=255)
	middle_name = models.CharField(max_length=255,blank=True)
	position = models.CharField(max_length=255)
	email = models.EmailField()
	phone = models.CharField(max_length=255)

	hear_about = models.TextField(verbose_name=_('How did you learn about Gèrye Jwa?'))
	support = models.TextField(verbose_name=_('How do you think Gerye Jwa can support your organization?'))
	more_info = models.TextField(blank=True,verbose_name=_('Any other information we should know?'))

class Interest(models.Model):
	name = models.CharField(max_length=255)
	name_fr = models.CharField(max_length=255)

	def __str__(self):
		return self.name

class GeryeJwa(models.Model):
	first_name = models.CharField(max_length=255)
	last_name = models.CharField(max_length=255)
	middle_name = models.CharField(max_length=255,blank=True)

	interests = models.ManyToManyField(Interest,blank=True)
	other_interest = models.CharField(max_length=255)

	address = models.CharField(max_length=255)
	street_address = models.CharField(max_length=255)
	address_line_2 = models.CharField(max_length=255)
	city = models.CharField(max_length=255)
	state = models.CharField(max_length=255)
	zip_code = models.CharField(max_length=255,verbose_name=_('ZIP / Postal Code'))

	email = models.EmailField()
	phone = models.CharField(max_length=255)

	hear_about = models.TextField(verbose_name=_('How did you learn about Gèrye Jwa?'))	

	timeframe = models.DateTimeField(max_length=255)
	criminal_background = models.FileField(upload_to='gerye_forms/')
	physical_limitations = models.TextField(verbose_name=_('Any physical limitations?'))	
	emergency_contact = models.TextField(verbose_name=_('In case of emergency contact'))
	resume = models.FileField(upload_to='gerye_forms/')	