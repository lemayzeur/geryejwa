from django import forms
from .models import Organization,GeryeJwa,Interest
from django.utils.translation import ugettext_lazy as _
from geryejwa.utils import add_class_to_field

from django.contrib.auth import get_user_model

class AccountForm(forms.Form):
	first_name = forms.CharField(max_length=255,
		 widget=forms.TextInput(attrs={'class':'form-control',"placeholder":_("First name"),
		 	'data-toggle':"tooltip",'title':_("What is the first name?"),'data-trigger':"focus","placeholder":_("First name")}))

	last_name = forms.CharField(max_length=255,
		 widget=forms.TextInput(attrs={'class':'form-control',"placeholder":_("Last name"),
		 	'data-toggle':"tooltip",'title':_("What is the last name?"),'data-trigger':"focus","placeholder":_("Last name")}))

	email = forms.EmailField(max_length=255,
		 widget=forms.EmailInput(attrs={'class':'form-control input-sm',"placeholder":_("Email")}))


	def clean(self):
		cleaned_data = super(LoginForm,self).clean()
		email = cleaned_data.get('email')
		
		for field_name,html_rendered in self.errors.items():
			add_class_to_field(self,field_name,'is-invalid')

class LoginForm(forms.Form):
	email = forms.EmailField(max_length=255,
		 widget=forms.EmailInput(attrs={'class':'form-control input-sm',"placeholder":_("Email")}))

	password = forms.CharField(max_length=130,
		widget=forms.PasswordInput(attrs={'class':'form-control input-sm signup-input', 'placeholder':_('Password')}))


	def clean(self):
		cleaned_data = super(LoginForm,self).clean()
		email = cleaned_data.get('email')
		password = cleaned_data.get('password')

		if not password:
			self.add_error('password',_('The password field is required'))

		if not email:
			self.add_error('email',_('Please provide your Email'))

		for field_name,html_rendered in self.errors.items():
			add_class_to_field(self,field_name,'is-invalid')

class RegisterForm(forms.Form):
	first_name = forms.CharField(max_length=255,required=False,
		 widget=forms.TextInput(attrs={'class':'form-control',"placeholder":_("First name"),
		 	'data-toggle':"tooltip",'title':_("What is your first name?"),'data-trigger':"focus","placeholder":_("First name")}))

	last_name = forms.CharField(max_length=255,required=False,
		 widget=forms.TextInput(attrs={'class':'form-control',"placeholder":_("Last name"),
		 	'data-toggle':"tooltip",'title':_("What is your last name?"),'data-trigger':"focus","placeholder":_("Last name")}))

	business_name = forms.CharField(max_length=255,required=False,
		 widget=forms.TextInput(attrs={'class':'form-control',"placeholder":"",
		 	'data-toggle':"tooltip",'title':_("Provide a business name"),'data-trigger':"focus","placeholder":_("Business name")}))

	# style_of_konpa = forms.CharField(max_length=255,required=False,
		 # widget=forms.TextInput(attrs={'class':'form-control input-sm signup-input',"placeholder":"Style of konpa"}))

	# style_of_mix = forms.CharField(max_length=255,required=False,
		 # widget=forms.TextInput(attrs={'class':'form-control input-sm signup-input',"placeholder":"Style of Mix"}))

	# street = forms.CharField(max_length=255,required=False,
		 # widget=forms.TextInput(attrs={'class':'form-control input-sm signup-input',"placeholder":"Street"}))

	email = forms.EmailField(max_length=200,required=False,
		 widget=forms.EmailInput(attrs={'class':'form-control input-sm signup-input',"placeholder":_("E-mail address"),
		 	'data-toggle':"tooltip",'title':_("You will need it when you log in and if you ever want to reset your password"),'data-trigger':"focus"}))

	phone = forms.CharField(max_length=20,required=False,
		 widget=forms.TextInput(attrs={'class':'form-control input-sm signup-input',"placeholder":_("Telephone number")}))

	# city = forms.CharField(max_length=255,required=False,
		 # widget=forms.TextInput(attrs={'class':'form-control input-sm signup-input',"placeholder":"City"}))

	# state = forms.CharField(max_length=255,required=False,
		 # widget=forms.TextInput(attrs={'class':'form-control input-sm signup-input',"placeholder":"State"}))

	# description = forms.CharField(required=False,
		 # widget=forms.Textarea(attrs={'class':'form-control signup-input',"rows":"6",'style':'resize:none'}))

	# interested_ticket_sale = forms.BooleanField(required=False,
		# widget=forms.CheckboxInput(attrs={'class':'custom-checkbox'}))

	# interested_ads = forms.BooleanField(required=False,
		# widget=forms.CheckboxInput(attrs={'class':'custom-checkbox'}))

	password = forms.CharField(max_length=130,required=False,
		widget=forms.PasswordInput(attrs={'class':'form-control input-sm signup-input', 'placeholder':_('Password'),
			'data-toggle':"tooltip",'title':_("Use 6 or more characters with a mix of letters, numbers"),'data-trigger':"focus"}))

	password_confirm = forms.CharField(max_length=130,required=False,
		widget=forms.PasswordInput(attrs={'class':'form-control input-sm signup-input', 'placeholder':_('Confirm the password')}))

	# signature = forms.CharField(max_length=255,required=False,
		 # widget=forms.TextInput(attrs={'class':'form-control input-sm signup-input',"placeholder":"Authorized signature"}))


	def clean(self):
		cleaned_data = super(RegisterForm,self).clean()
		business_name = cleaned_data.get('business_name')
		first_name = cleaned_data.get('first_name')
		last_name = cleaned_data.get('last_name')
		
		email = cleaned_data.get('email')
		phone = cleaned_data.get('phone')
		password = cleaned_data.get('password')
		password_confirm = cleaned_data.get('password_confirm')

		if not first_name:
			self.add_error("first_name",_("required"))

		if not last_name:
			self.add_error("last_name",_("required"))


		if not email:
			self.add_error("email",_("required"))
		elif get_user_model().objects.filter(email__iexact=email).exists():
			self.add_error("email",_("This email is already taken"))

		if not phone:
			self.add_error('phone','required')

		if not password:
			self.add_error('password','required')

		elif password and password_confirm:
			if password != password_confirm:
				self.add_error('password',_('The passwords are not same'))
			elif len(password) < 6:
				self.add_error('password',_('Weak password, at least 6 digits'))
			#elif self.is_password_vulnerable(password): Comprend que des chiffres ou des lettres
				#self.add_error('password','Mot de passe vulnerable, combinez des chiffres et des lettres')

			if business_name:
				if password.lower() in business_name.lower():
					self.add_error('password',_('Password contains your names'))

			if first_name and last_name:
				if password.lower() in first_name.lower() or password.lower() in last_name.lower():
					self.add_error('password',_('Password contains your names'))
			
		if not password_confirm:
			self.add_error('password_confirm',_("required"))	

		for field_name,html_rendered in self.errors.items():
			add_class_to_field(self,field_name,'is-invalid')

class ForgotAccountForm(forms.Form):
	email = forms.EmailField(max_length=255,
		 widget=forms.EmailInput(attrs={'class':'form-control input-sm',"placeholder":_("Email")}))

	def clean(self):
		cleaned_data = super(ForgotAccountForm,self).clean()
		email = cleaned_data.get('email')

		
		if not email:
			self.add_error('email',_('Please provide your email'))
		elif not get_user_model().objects.filter(email__iexact=email).exists():
			self.add_error("email",_("Your email doesn't exist on our platform"))

		for field_name,html_rendered in self.errors.items():
			add_class_to_field(self,field_name,'is-invalid')

class FileForm(forms.Form):
	file = forms.FileField(required=False,
		 widget=forms.FileInput(attrs={'class':'form-control','style':'display:none'}))

	def clean(self):
		cleaned_data = super(FileForm,self).clean()
		file = cleaned_data.get("file")

		if file:
			if file.size > MAX_SIZE_FILE:
				self.add_error('file',_('File too big, max size is %(MAX_SIZE_FILE)s') % {"MAX_SIZE_FILE":c_bytes(MAX_SIZE_FILE)})
		else:
			self.add_error("file",_("required field"))


class PhotoForm(forms.Form):
	photo = forms.ImageField(required=False,
		 widget=forms.FileInput(attrs={'class':'form-control','style':'display:none'}))
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	rotate = forms.FloatField(widget=forms.HiddenInput())
	box = forms.CharField(widget=forms.HiddenInput())

	def clean(self):
		cleaned_data = super(PhotoForm,self).clean()
		# title = cleaned_data.get('title')
		photo = cleaned_data.get('photo')#
		x = cleaned_data.get('x')
		y = cleaned_data.get('y')
		w = cleaned_data.get('width')
		h = cleaned_data.get('height')
		r = cleaned_data.get('rotate')
		box = cleaned_data.get('box')


		#cropped_image = image.crop((x, y, w+x, h+y))
		#resized_image = cropped_image.resize((200, 200), Image.ANTIALIAS)
		#resized_image.save(photo.file.path)

		if photo:
			if photo.size > MAX_SIZE_PHOTO:
				self.add_error('photo',_('File too big, max size is %(MAX_SIZE_PHOTO)s') % {"MAX_SIZE_PHOTO":c_bytes(MAX_SIZE_PHOTO)})
			else:
				boxX , boxY = int(box.split("x")[0]),int(box.split("x")[1])
				photo = CompressImage(photo) # Initialize the image
				if r:
					photo = photo.rotate(r)
				photo = photo.crop(size=(boxX,boxY),box=(x, y, w+x, h+y))

				# from django.core.files.images import get_image_dimensions
				W,H = photo.dimensions()
				if W > boxX or H > boxY:
					photo = photo.resize(size=(boxX,boxY))
				elif(W - 50) > boxX or (H - 50) > boxY:
					photo = photo.resize(size=(boxX,boxY))
				cleaned_data['photo'] = photo.render()

		else:
			self.add_error("photo",_("required"))

		for field_name,html_rendered in self.errors.items():
			add_class_to_field(self,field_name,'is-invalid')

class ResetPasswordForm(forms.Form):
	def __init__(self, user, *args, **kwargs):
		self.user = user
		self.first_name = user.first_name
		self.last_name = user.last_name
		self.business_name = user.profile.business_name
		super(ResetPasswordForm, self).__init__(*args, **kwargs)

	
	new_password = forms.CharField(max_length=130,required=False,
		widget=forms.PasswordInput(attrs={'class':'form-control input-sm signup-input', 'placeholder':_('New password'),
			'data-toggle':"tooltip",'title':_("Use 6 or more characters with a mix of letters, numbers"),'data-trigger':"focus"}))

	new_password_confirm = forms.CharField(max_length=130,required=False,
		widget=forms.PasswordInput(attrs={'class':'form-control input-sm signup-input', 'placeholder':_('Confirm the new password')}))

	def clean(self):
		cleaned_data = super(ResetPasswordForm,self).clean()
		
		new_password = cleaned_data.get('new_password')
		new_password_confirm = cleaned_data.get('new_password_confirm')
		first_name = self.first_name
		last_name = self.last_name
		business_name = self.business_name

		
		if self.user.check_password(new_password):
			self.add_error('new_password',_("That's funny! You provided exactly the same password as before"))

		if not new_password:
			self.add_error('new_password',_("required"))

		elif new_password and new_password_confirm:
			if new_password != new_password_confirm:
				self.add_error('new_password',_('The passwords are not same'))
			elif len(new_password) < 6:
				self.add_error('new_password',_('Weak password, at least 6 digits'))
			#elif self.is_password_vulnerable(password): Comprend que des chiffres ou des lettres
				#self.add_error('password','Mot de passe vulnerable, combinez des chiffres et des lettres')

			if business_name:
				if new_password.lower() in business_name.lower():
					self.add_error('new_password',_('Password contains your names'))

			if first_name and last_name:
				if new_password.lower() in first_name.lower() or new_password.lower() in last_name.lower():
					self.add_error('new_password',_('Password contains your names'))
			
		if not new_password_confirm:
			self.add_error('new_password_confirm',_("required"))	

		for field_name,html_rendered in self.errors.items():
			add_class_to_field(self,field_name,'is-invalid')

		return cleaned_data


class GeryeJwaForm(forms.ModelForm):
	interests_ = forms.BooleanField()

	interests = forms.ModelMultipleChoiceField(queryset=Interest.objects.all(),
		widget=forms.CheckboxSelectMultiple(attrs={'class':'list-unstyled ml-3'}))

	class Meta:
		model = GeryeJwa 
		fields = '__all__'

	def __init__(self, *args, **kwargs):
		super(GeryeJwaForm, self).__init__(*args, **kwargs)
		self.fields['other_interest'].required = True
		self.fields['first_name'].widget = forms.TextInput(attrs={
			'class': 'form-control'})

		self.fields['other_interest'].widget = forms.TextInput(attrs={
			'class': 'form-control form-control-sm',' style':'width: 50%','placeholder':"Other interest"})


		self.fields['hear_about'].widget = forms.Textarea(attrs={
			'class': 'form-control no-resize',
			'rows':'3',
			})
		self.fields['emergency_contact'].widget = forms.Textarea(attrs={
			'class': 'form-control no-resize',
			'rows':'2',
			})
		self.fields['physical_limitations'].widget = forms.Textarea(attrs={
			'class': 'form-control no-resize',
			'rows':'2',
			})

		for i in ['first_name','middle_name','last_name','address','street_address',
			'address_line_2','city','state','zip_code','email','phone','timeframe']:
			self.fields[i].widget = forms.TextInput(attrs={
				'class': 'form-control',
				})


		for key,field in self.fields.items():
			field.widget.attrs['placeholder'] = field.label

	def clean(self):
		form_data = super(GeryeJwaForm,self).clean()
		interests = form_data.get("interests")
		interests_ = form_data.get("interests_")
		if not interests and not interests_:
			self.add_error("interests",'Choose one or more interests')

		if interests_ and not other_kind:
			self.add_error("other_interest",'required field')
		for field_name,html_rendered in self.errors.items():
			add_class_to_field(self,field_name,'is-invalid')

		return form_data

class OrganizationForm(forms.ModelForm):
	other_kind = forms.CharField(max_length=255,required=False,
		 widget=forms.TextInput(attrs={'class':'form-control',"placeholder":_("Other type of organization")}))

	class Meta:
		model = Organization 
		fields = '__all__'

	def __init__(self, *args, **kwargs):
		super(OrganizationForm, self).__init__(*args, **kwargs)
		self.fields['name'].widget = forms.TextInput(attrs={
			'class': '',
			'placeholder': _('Organization name')})

		self.fields['mission'].widget = forms.Textarea(attrs={
			'class': 'no-resize',
			'placeholder': _('Mission'),
			'rows':'2',
			})

		for key,field in self.fields.items():
			field.widget.attrs['class'] = 'form-control'
			field.widget.attrs['placeholder'] = field.label
			if field.widget == forms.Textarea():
				field.widget.attrs['rows'] = '2'

	def clean(self):
		form_data = super(OrganizationForm,self).clean()
		kind = form_data.get("kind")
		if kind == '-' and not other_kind:
			self.add_error("other_kind",'required field')
		for field_name,html_rendered in self.errors.items():
			add_class_to_field(self,field_name,'is-invalid')

		return form_data