from django.urls import path,include,re_path
from . import views


urlpatterns = [
    path('', views.index,name='index'),
    path('login/', views.signin,name='signin'),
    path('signout/',views.signout,name='signout'),
    path('forgot-acc/',views.forgotAccount,name="forgotAccount"),
    path('setlang/',views.set_language,name='set_language'),
    
    re_path(r'^acc-conf/(?P<activation_key>\w{40})/$',views.accountConfirmation,name="accountConfirmation"),
    path('acc-conf-expired/',views.accountConfirmationExpired,name="accountConfirmationExpired"),
    path('send-email/',views.sendEmail,name="sendEmail"),

    re_path(r'^token-conf/(?P<password_token>\w{30})/$',views.passwordTokenConfirmation,name="passwordTokenConfirmation"),
    path('token-conf-expired/',views.passwordTokenConfirmationExpired,name="passwordTokenConfirmationExpired"),
    path('reset-pswd/',views.resetPassword,name="resetPassword"),

    path('about/', views.about,name='about'),
    path('about/story/', views.story,name='story'),
    path('team/', views.team,name='team'),
    path('contact/', views.contact,name='contact'),
    path('involve/', views.involve,name='involve'),
    path('program/', views.program,name='program'),
    path('community/', views.community,name='community'),
    path('donate/', views.donate,name='donate'),
    path('other-donation/', views.otherDonation,name='otherDonation'),
    path('partners', views.partners,name='partners'),
    path('partners/form', views.partnerForm,name='partnerForm'),
    path('partners/form/review', views.partnerFormReview,name='partnerFormReview'),
    path('become-a-gerye-jwa', views.becomeGeryeJwa,name='becomeGeryeJwa'),
    path('volunteer', views.volunteer,name='volunteer'),
    path('gerye-jwa/form/review', views.partnerFormReview,name='partnerFormReview'),
    path('stats/institutions', views.institutions,name='institutions'),
    path('gallery/children', views.galleryChildren,name='galleryChildren'),
    path('map', views.map,name='map'),
]
