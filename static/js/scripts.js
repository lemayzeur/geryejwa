var DEBUG = $('input[name=DEBUG]').val();
var OFFSET_TOP = $('nav.navbar').height() + $("#navbar-top").height();
$(document).on("focus , click",'[data-toggle="datetimepicker"] , .datetimepicker',function(){
    var e = this;
    var startDate = $(e).data('start'),
        endDate = $(e).data('end'),
        format = $(e).data('format'),
        zIndex = $(e).attr('z-index');
    if(typeof(format) == 'undefined')
        format = 'DD/MM/YYYY hh:mm A';
    if(typeof(zIndex) == 'undefined')
        zIndex = 1050;

    $(e).datetimepicker({
        // autoHide:true,
        format: format,
        // zIndex: zIndex,
        // zIndex:zIndex,
        viewMode:'years',
        // showTodayButton:true,
        // showClose:true,
        // keepOpen:false,
        minDate:startDate,
        maxDate:endDate,
        showClear:true,
        // allowInputToggle:true,
        tooltips: {
            today: 'Go to today',
            clear: 'Effacer selection',
            close: 'Close the picker',
            selectMonth: 'Select Month',
            prevMonth: 'Previous Month',
            nextMonth: 'Next Month',
            selectYear: 'Select Year',
            prevYear: 'Previous Year',
            nextYear: 'Next Year',
            selectDecade: 'Select Decade',
            prevDecade: 'Previous Decade',
            nextDecade: 'Next Decade',
            prevCentury: 'Previous Century',
            nextCentury: 'Next Century'
        }       
    });
});
$(document).on("click",'.call-datetimepicker',function(){
    $(this).closest("div").find('.datetimepicker').focus();
});

$('.countabled').each(function () {
    var $this = $(this);
    jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
        duration: 1000,
        easing: 'swing',
        step: function () {
            $this.text(Math.ceil(this.Counter));
        }
    });
});

// ring('dark','Une nouvelle biographie a ete ajoutee','Une biographie concernant la vie de Jean Jean Roosevelt vient d')
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
$(function () {
    $('[data-toggle="popover"]').popover({
        trigger: 'focus'
    })
});

function ring(type,title,msg,position=['bottom','left'],url="#"){
    $.notify({
        icon: 'fa fa-book kiyeski-color',
        title: '<span class="jura f-16">'+title+'</span><br>',
        url:url,
        message: "<span class='f-13'>"+msg+"</span>",
    },{
        type: type,
        placement: {
            from: position[0],
            align: position[1],
        },
        z_index: 3031,
        delay: 10000,
        animate: {
            enter: 'animated fadeInUp',
            exit: 'animated fadeOutUp'
        },
    });
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function setCookie(name,value,max_age=24*60*60) {
    var d = new Date();
    d.setTime(d.getTime() + (max_age*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}
function deleteCookie(name) {
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function updateQuery(key,value){
    var url = [location.protocol, '//', location.host, location.pathname].join('');
    var query = window.location.search.substring(1);
    var query_object = {};
    var new_query = ''
    var j = 0

    if(query){
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            if(vars[i]){
                var pair = vars[i].split("=");
                if(typeof(pair[1]) === 'undefined'){
                    pair[1] = '';
                }
                query_object[pair[0]] = decodeURIComponent(pair[1]);
            }
        }
    }
    query_object[key] = value;

    for (var k in query_object){
        if(query_object[k])
            new_query = new_query + k + "=" + query_object[k];
        else
            new_query = new_query + k;
            
        if(j < Object.keys(query_object).length - 1){
            new_query += "&";
            j += 1;
        }
    }
    window.history.pushState("","","?"+new_query);

    $(".update-href").each(function(i,e){
        $(e).attr("href",$(e).attr("save-url")+new_query);
    });
    // return url + new_query
}

function xmlData(data){
    var el = document.createElement('html');
    el.innerHTML = "<html><head><title>titleTest</title></head><body>"+data+"</body></html>";
    return el;
}


function gcShow(part){
    if(!part){
        if($("#media-carousel").is(':visible')){
            $("#media-carousel").hide();
            $("#media-player").show();
            updateQuery('gc','mp');
        }else{
            $("#media-carousel").show();
            $("#media-player").hide();
            updateQuery('gc','mc');
        }
    }else if(part == 'mp'){
        $("#media-carousel").hide();
        $("#media-player").show();
        updateQuery('gc','mp');

    }else if(part == 'mc'){
        $("#media-carousel").show();
        $("#media-player").hide();
        updateQuery('gc','mc');
    }
}

function objectToHtmlElement(elemen){
    var node = document.createElement(elemen.tag);
    node.innerHTML = elemen.content;
    for (var attr in elemen.attr) {
      node.setAttribute(attr, elemen.attr[attr]);
    }
    for (var prop in elemen.events) {
      node.addEventListener(prop, new Function(elemen.events[prop]).bind(node));
    }
    for(var keys in elemen.style){
        node.setAttribute(keys,elemen.style[keys])
    }

    return node;
}

(function(){
    /* POU NOU KA EFASE TOUT TEKS ERE YO DEPI MOUN NAN KOMANSE TAPE YON LOT MODPAS */
    $(document).on("keypress , change",".has-error .form-control",function(e){ 
       
        if(e.keyCode != 9){
            $(this).removeClass('is-invalid');
            $(this).closest(".has-error").find("div[class^=invalid]").css({"height":"14px"}).text("");
            $(this).closest(".has-error").removeClass("has-error text-danger");
        }
    });
    /* POU NOU KA EFASE TOUT TEKS ERE YO DEPI MOUN NAN KOMANSE TAPE YON LOT MODPAS */
    $(document).on("click","input[type=radio] , input[type=checkbox]",function(e){ 
        $(this).removeClass('is-invalid');
        $(this).closest(".has-error").find("div[class^=invalid]").css({"height":"15px"}).text("");
        $(this).closest(".has-error").removeClass("has-error text-danger");
    });

    $("ea")
})();
(function(){
    $(".fitscreen").each(function(i,e){
        var height = 'calc(100vh - ' + OFFSET_TOP + 'px)';
        $(e).css('height',height);
    });
})();


/* CROP IMAGE */
(function($) {
    var title,cancel,upload,sectionReplace,spin,
        height,width,ratio,json,url,photo;

    var triggerPhoto, cropThisPhoto;

    var MAX_SIZE_PHOTO = 7 * 1024 * 1024;

    if (window.XMLHttpRequest) {
        // code for modern browsers
        var ajax = new XMLHttpRequest();
    } else {
        // code for old IE browsers
        var ajax = new ActiveXObject("Microsoft.XMLHTTP");
    } 

    function main(that){

        sectionReplace = $(that).data("replace");
        if(sectionReplace.indexOf(',') < 0)
            spin = sectionReplace+"-spin"; else spin = undefined;
        url = $(that).data("url");
        width = $(that).data("width");
        height = $(that).data("height");
        ratio = $(that).data("ratio");
        title = $(that).data("title");
        cancel = $(that).data("cancel");
        upload = $(that).data("upload");
        json = $(that).data("json");

        if(!csrftoken){
            alert("Please reload the page to ensure that the cookies are actually set up");
        }
        upload = $(that).data("upload");
        cancel = $(that).data("cancel");

        var bridge = true;

        if(typeof(url) == 'undefined'){
            alert("Error: data-url is undefined");
            bridge = false;
        }
        else if(typeof(height) == 'undefined'){
            alert("Error: data-height is undefined");
            bridge = false;
        }
        else if(typeof(width) == 'undefined'){
            alert("Error: data-width is undefined");
            bridge = false;
        }
        if(!ratio){
            ratio = width/height;
        }
        if(!title){
            title = 'Crop the photo to fit';
        }
        
        if(!upload){
            upload = "Crop and upload";
        }
        if(!cancel){
            cancel = "Cancel";
        }
        if(!url){
           url = '?ref=crop'
        }else{
            url += "&ref=crop";
        }
        if(bridge)
           return true;
        return false;
    }
    function htmlCrop(){
        var html = '<div class="modal fade" id="modalCrop" tabindex="-1" role="dialog" aria-labelledby="modalCrop" aria-hidden="true">\
            <div class="modal-dialog modal-lg" role="document">\
                <div class="modal-content" style="position:relative">\
                    <div class="modal-header">\
                        <h4 class="modal-title">Lorry</h4>\
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                        <span aria-hidden="true">&times;</span>\
                        </button>\
                    </div>\
                    <div class="modal-body" style="position:relative">\
                        <div class="modal-progress-bar progress middle " style="display:none;position:absolute;top:0;z-index:1340;width:100%;border-radius:0">\
                            <div class="progress-bar progress-bar-success active progress-bar-striped" role="progressbar" aria-valuenow="0"\
                            aria-valuemin="0" aria-valuemax="100" style="width:0">0%</div>\
                        </div> \
                        <div class="overlay modal-overlay" style="display:none"></div>\
                        <img src="" id="imagePO" style="max-width: 100%;height:450px">\
                    </div>\
                    <div class="modal-footer left">\
                        <div class="btn-group btn-trigger float-left" role="group">\
                            <button type="button" class="btn btn-sm btn-warning js-zoom-in">\
                                <span class="fa fa-search-plus"></span>\
                            </button>\
                            <button type="button" class="btn btn-sm btn-warning js-zoom-out">\
                                <span class="fa fa-search-minus"></span>\
                            </button>\
                        </div>\
                        <div class="btn-group btn-trigger float-left" role="group">\
                            <button type="button" class="btn btn-sm btn-secondary js-move-left">\
                                <span class="fa fa-chevron-left"></span>\
                            </button>\
                             <button type="button" class="btn btn-sm btn-secondary js-move-right">\
                                <span class="fa fa-chevron-right"></span>\
                            </button>\
                             <button type="button" class="btn btn-sm btn-secondary js-move-down">\
                                <span class="fa fa-chevron-down"></span>\
                            </button>\
                            <button type="button" class="btn btn-sm btn-secondary js-move-up">\
                                <span class="fa fa-chevron-up"></span>\
                            </button>\
                        </div>\
                        <div class="btn-group btn-trigger float-left" role="group">\
                            </button>\
                             <button type="button" class="btn btn-sm btn-warning js-rotate-left">\
                                <span class="fa fa-undo"></span>\
                            </button>\
                            <button type="button" class="btn btn-sm btn-warning js-rotate-right">\
                                <span class="fa fa-repeat"></span>\
                            </button>\
                        </div>\
                        <div class="btn-group btn-trigger float-left hidden" role="group">\
                            <button type="button" class="btn btn-sm btn-danger js-drag-mode">\
                                <span class="fa fa-arrows"></span>\
                            </button>\
                        </div>\
                        <div class="float-right">\
                        <button type="button" class="btn btn-sm btn-danger modal-cancel" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>\
                        <button type="button" class="btn btn-sm btn-secondary modal-upload-photo"><i class="fa fa-upload"></i> Choose another photo</button>\
                        <button type="button" class="btn btn-sm btn-primary modal-upload js-crop-and-upload"><i class="fa fa-send"></i> Upload</button>\
                    </div></div>\
                </div>\
            </div>\
        </div>';
        $(document.body).append(html);
    }
    htmlCrop();
   /* if(typeof($("#modalCrop") == 'undefined')*/
        
    $(document).on("change",".crop-this-photo",function () {
        photo = this.files[0];
        cropThisPhoto = this;
        triggerPhoto = $(this).attr("trigger");

        if(photo.size <= MAX_SIZE_PHOTO){
            if(main(this)){
                if (this.files && this.files[0]) {
                    if(spin != undefined)
                        $(spin).show();
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePO").attr("src", e.target.result);
                        $("#modalCrop .modal-title").html(title);
                        $("#modalCrop .modal-upload").html(upload);
                        $("#modalCrop .modal-cancel").html(cancel);

                        $("#modalCrop").modal("show");
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            }else{
                ring("danger","Something went wrong");
            }
        }else{
            ring("danger","File ["+photo.name+"] with "+c_bytes(photo.size)+" is too big, max size is "+c_bytes(MAX_SIZE_PHOTO))
        }
       
    });
    var $image = $("#imagePO");
    var internet_slow;
    var file_too_big;

    var cropBoxData;
    var canvasData;
    $(document).on("shown.bs.modal","#modalCrop", function () {
        $image.cropper({
            viewMode: 1,
            dragMode:"move",
            restore:false,
            autoCropArea: 0.5,
            aspectRatio: ratio,
            minCropBoxWidth: 100,
            minCropBoxHeight: 100,
            ready: function () {
                $image.cropper("setCanvasData", canvasData);
                $image.cropper("setCropBoxData", cropBoxData);
            }
        });
    })
    $(document).on("hidden.bs.modal","#modalCrop",function () {
        // cropBoxData = $image.cropper("getCropBoxData");
        // canvasData = $image.cropper("getCanvasData");
        $image.cropper("destroy");
        $("#imagePO").attr("src","");
        cropThisPhoto.value = "";
        abortButton();

        $("div.modal-progress-bar").hide();
        $("div.modal-overlay").hide();
        $("div.modal-progress-bar .progress-bar").attr("aria-valuenow",0);
        $("div.modal-progress-bar .progress-bar").css({"width":0});
        $("div.modal-progress-bar .progress-bar").text("0");
    });

    // Enable zoom in button
    $(document).on("click",".js-zoom-in",function () {
        $image.cropper("zoom", 0.1);
    /*  var cropData = $image.cropper("getData");
        alert(cropData["y"]);*/
    });

    // Enable zoom out button
    $(document).on("click",".js-zoom-out",function () {
        $image.cropper("zoom", -0.1);
    });
    $(document).on("click",".js-move-up",function () {
        $image.cropper("move", 0,7);
    });
    $(document).on("click",".js-move-down",function () {
        $image.cropper("move", 0,-7);
    });
    $(document).on("click",".js-move-left",function () {
         $image.cropper("move", 7,0);
    });
    $(document).on("click",".js-move-right",function () {
         $image.cropper("move", -7,0);
    });
     $(document).on("click",".js-rotate-left",function () {
         $image.cropper("rotate", -90);
    });
    $(document).on("click",".js-rotate-right",function () {
         $image.cropper("rotate", 90);
    });

    $(document).on("click",".js-drag-mode",function () {
        if($(this).find("span").hasClass("fa-arrows")){
            $(this).find("span").removeClass("fa-arrows").addClass("fa-crop");
            $image.cropper({"setDragMode":"move"});
        }else{
             $(this).find("span").removeClass("fa-crop").addClass("fa-arrows");
             $image.cropper({"setDragMode":"crop"});
        }        
    });

    $(document).on("click",".modal-upload-photo",function () {
        clearTimeout(internet_slow);
        clearTimeout(file_too_big);
        $image.cropper("destroy");
        $("#modalCrop").modal('hide');
        $(triggerPhoto).click();
         
    });

    $(document).on("click",".js-crop-and-upload",function () {
        internet_slow = setTimeout(function(){
            timeOut();
        },150000);

        file_too_big = setTimeout(function(){
            fileTooBig();
        },90000);

        $("div.modal-progress-bar").show();
        $("div.modal-overlay").show();

        $(".btn-trigger button").attr('disabled',true).css("cursor","default");
        $(".modal-upload-photo").attr('disabled',true).css("cursor","default");


        $(this).attr('disabled',true).css("cursor","default");
        var cropData = $image.cropper("getData");
        var box = width.toString()+"x"+height.toString();
        
        var formData = new FormData();
        formData.append("photo",photo);
        formData.append("csrfmiddlewaretoken",csrftoken);
        formData.append("x",cropData["x"]);
        formData.append("y",cropData["y"]);
        formData.append("width",cropData["width"]);
        formData.append("height",cropData["height"]);
        formData.append("rotate",cropData["rotate"]);
        formData.append("box",box);

        for (var key in json) {
            if (json.hasOwnProperty(key)) {
                formData.append(key,json[key]);
            }
        }   

        ajax.upload.addEventListener("progress", progressHandler, false);
        ajax.addEventListener("load", completeHandler, false);
        ajax.addEventListener("error", errorHandler, false);
        ajax.addEventListener("abort", abortHandler, false);
        ajax.addEventListener("loadend", loadEndHandler, false);
        ajax.open("POST", url+"&id=" + Math.random(),true);
        ajax.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        // http://www.developphp.com/video/JavaScript/File-Upload-Progress-Bar-Meter-Tutorial-Ajax-PHP
        //use file_upload_parser.php from above url
        ajax.send(formData);
    });

    function _(el) {
        return document.getElementById(el);
    }
    function timeOut(){
       ring("info","Votre internet semble trop lent, vous pouvez attendre jusqu'à la fin du téléchargement");
    }
    function fileTooBig(){
       ring("info","Ce fichier ["+photo.name+"] pèse environ "+c_bytes(photo.size)+", cela peut prendre du temps à télécharger");
    }

    function abortButton(){
        ajax.abort();
    }

    function loadEndHandler(e) {
        // console.log("The transfer finished (although we don't know if it succeeded or not).");
        $(".js-crop-and-upload").attr('disabled',false).css("cursor","pointer");
        $(".btn-trigger button").attr('disabled',false).css("cursor","pointer");
        $(".modal-upload-photo").attr('disabled',false).css("cursor","pointer");
        $(spin).hide();
        clearTimeout(internet_slow);
        clearTimeout(file_too_big);
    }

    function progressHandler(event) {
        var percent = (event.loaded / event.total) * 100;
        $("div.modal-progress-bar .progress-bar")
        .attr("aria-valuenow",Math.round(percent)).css({"width":Math.round(percent) + "%"})
        .text(Math.round(percent) + "%");
    }   

    function completeHandler(event) {
        var data = xmlData(event.target.responseText);
        if(data['status'] == 'ok'){
            window.location.href = "";
        }
        // $(sectionReplace).replaceWith($(sectionReplace,$(data)));
        if(sectionReplace){
            lor = sectionReplace.split(",");
            for(var i = 0 ; i < lor.length ; i++ ){
                $(lor[i]).replaceWith($(lor[i],$(data)));
            }
        }else{
            window.location.href = "";
        }
        $("#modalCrop").modal('hide');
        clearTimeout(internet_slow);
        clearTimeout(file_too_big);
    }

    function errorHandler(event) {
        // $(".js-crop-and-upload").attr('disabled',false).css("cursor","pointer");
    }

    function abortHandler(event) {
        $(".js-crop-and-upload").attr('disabled',false).css("cursor","pointer");
        $(".btn-trigger button").attr('disabled',false).css("cursor","pointer");
        $(".modal-upload-photo").attr('disabled',false).css("cursor","pointer");
    }
    
}(jQuery));

function ring(type,msg,position=['top','right'],url="#"){
    $.notify({
        icon: 'glyphicon glyphicon-bell',
        title: '<strong>Notification</strong><br>',
        url:url,
        message: msg,
    },{
        type: type,
        placement: {
            from: position[0],
            align: position[1],
        },
        z_index: 3031,
        delay: 20000,
        animate: {
            enter: 'animated bounceInLeft',
            exit: 'animated fadeOutUp'
        },
    });
}

/* DELETE OBJECT */
(function($) {
    var link,msg,ajax,json,csrf,
        method,replace,yes,no,url,that,classes;

    function onDelete(msg,yes,no){
        var htmlpur = '<div class="modal fade in" id="myModal">\
            <div class="modal-dialog modal-sm">\
                <div class="modal-content">\
                    <div class="modal-header bg-warningr">\
                        <h5 class="modal-title style="text-align:center">Confirmation</h5>\
                        <button type="button" class="close" data-dismiss="modal">&times;</button>\
                    </div>\
                    <div class="modal-body">\
                        <div class="row" style="text-align:center">\
                            <div class="col-sm-12">\
                               '+msg+'\
                            </div>\
                        </div>\
                    </div>\
                    <div class="modal-footer">\
                         <button id="btnYes" class="btn btn-danger btn-sm"><i class="fa fa-check"></i> '+yes+'</button>\
                         <button data-dismiss="modal" aria-hidden="true" class="btn btn-secondary btn-sm"><i class="fa fa-remove"></i> '+no+'</button>\
                    </div>\
                </div>\
            </div>\
        </div>';
        $(document.body).append(htmlpur);
        $("#myModal").modal("show");
    }
    $(document).on("click",".delete-object",function(e){
        that = this;
        $(this).attr('disabled',true);
        classes =  $(this).find(".fa").attr("class");
        $(this).find(".fa").attr("class","fa fa-spin fa-spinner");

        e.preventDefault();
        link = $(this).data("link");
        url = $(this).data("url");
        msg = $(this).data("msg");
        ajax = $(this).data("ajax");
        json = $(this).data("json"); // "{'w':'delete','id':3}"
        method = $(this).data("method");
        replace = $(this).data("replace");
        csrf = getCookie("csrftoken");
        if(!csrf){
            alert("Please reload the page to ensure that the cookies are actually set up");
        }
        yes = $(this).data("yes");
        no = $(this).data("no");

        var bridge = true;

        if(typeof(url) == 'undefined'){
            alert("Error: data-url is undefined");
            bridge = false;
        }
   
        if(typeof(link) == 'undefined' && !ajax){
            alert("Error: data-link is undefined, this link serves to redirect after the process, if ajax is false");
            bridge = false;
        }
        if(!msg){
            msg = '<p>You are about to delete.</p>\
                <p>Do you want to proceed?</p>';
        }
        if(typeof(ajax) != 'undefined' && ajax){
            if(typeof(json) == 'undefined'){
                alert("Error: data-json must receive a JSON String data");
                bridge = false;
            }
            
            if(!method){
               method = 'get';
            }
            if(!replace){
                replace = null;
            }
            if(!link){
               link = "";
            }
        }else{
            json = {};
            csrf = $("input[name=csrfmiddlewaretoken]").val();
            replace = null;
            method = 'get';
        }
        if(!yes){
            yes = "Yes";
        }
        if(!no){
            no = "No";
        }
        if(bridge)
            onDelete(msg,yes,no);
    });

    $(document).on('hidden.bs.modal',"#myModal",function() {
        $(this).remove();
        $(that).attr('disabled',false);
        $(that).find(".fa").attr("class",classes);
    });
    $(document).on('shown.bs.modal',"#myModal",function() {
        $that = $(this);
        $('#btnYes').click(function() {
            $(this).attr("disabled",true).css("cursor",'default');
            data = json
            data["csrfmiddlewaretoken"] = getCookie("csrftoken");
            $that.modal('hide');

            if(ajax){
                $.ajax({
                    url:url,
                    type:method,
                    data:data,
                    success:function(data){
                        $(that).attr('disabled',false);
                        $(that).find(".fa").attr("class",classes);

                        $('#myModal').modal('hide');
                        $('#btnYes').attr("disabled",false).css("cursor",'pointer');
                        if(replace){
                            lor = replace.split(",");
                            for(var i = 0 ; i < lor.length ; i++ ){
                                $(lor[i]).replaceWith($(lor[i],$(data)));
                            }
                        }else{
                            window.location.href = link;
                        }


                        /*$.notify({
                            icon: 'glyphicon glyphicon-bell',
                            title: '<strong>Message from Atlantis</strong><br>',
                            message: 'Process successfully completed'
                        },{
                            type: 'success',
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            z_index: 3031,
                            delay: 20000,
                            animate: {
                                enter: 'animated bounceInLeft',
                                exit: 'animated fadeOutUp'
                            },
                        });*/
                        
                    },
                    error:function(){
                        $(that).attr('disabled',false);
                        $(that).find(".fa").attr("class",classes);

                        alert("One action is pending...");
                        $('#btnYes').attr("disabled",false).css("cursor",'pointer');
                    }
                })
            }else{
                if(link)
                    window.location.href = link;
                window.location.href = "";
            }

        });
    });
}(jQuery));

/* SEND WITHOUT PROMPTING */
(function($) {

    $(document).on("click",".hb",function(e) {
        

        var that = this;
        var classes =  $(this).find(".fa").attr("class");
        $(this).attr('disabled',true);
        $(this).find(".fa").attr("class","fa fa-spin fa-spinner");

        if($(that).attr("href")){
            e.preventDefault();
            
        }

        var data = $(this).data("json");
        var next = $(this).data("next");
        var link = $(this).data("link");
        var url = $(this).data("url");
        var method = $(this).data("method");
        var values = $(this).attr("add-values");

        if(values){
            values = JSON.parse(values);
            for(var key in values){
                values[key] = $(values[key]).val();
            }

            data = $.extend(data,values)
        }

        if(typeof(method) == undefined || !method){
            method = 'post';
        }

        if(typeof(data) == undefined || !data){
            alert("[json] is not defined");
            $(that).attr('disabled',false);
            $(that).find(".fa").attr("class",classes);
            return;
        }

        else if(typeof(next) == 'undefined'){
            alert("[next] is not defined [Link, Object or [modal]:Modal]");
            $(that).attr('disabled',false);
            $(that).find(".fa").attr("class",classes);
            return;
        }
        else if(next[0] != '.' && next[0] != '#' && next.slice(0,7) != '[modal]'){
            alert("There 's no 'class' nor 'id' nor [modal] from [next]");
            $(that).attr('disabled',false);
            $(that).find(".fa").attr("class",classes);
            return;
        }
        data["csrfmiddlewaretoken"] = getCookie("csrftoken");



        $.ajax({
            url:url,
            type:method,
            data:data,
            success:function(data){
                $(that).attr('disabled',false);
                $(that).find(".fa").attr("class",classes);

                if(data.denied){
                    ring("warning",data.msg)
                    return;
                }
                if(next){
                    if(next.startsWith('[modal]')){
                        $(next.slice(8)).modal("show");
                        return;
                    }
                    lor = next.split(",");
                    for(var i = 0 ; i < lor.length ; i++ ){
                        if(typeof(data) === 'string'){
                            data = xmlData(data);
                        }
                        
                        $(lor[i]).replaceWith($(lor[i],data));
                    }
                }else{
                    window.location.href = link;
                }
                if($(that).data("nofeedback")) return;
                ring("success",'successful set up')
            },
            error:function(){
                $(that).attr('disabled',false);
                $(that).find(".fa").attr("class",classes);

                ring("danger","We can't reach the server with that processs");
            }
        });
    });
}(jQuery));

/* SUBMIT FORM WITHOUT PROMPTING */
(function($) {
    $(document).on("click",".sage-form",function(e) {        
        var that = this;
        var classes =  $(this).find(".fa").attr("class");
        $(this).attr('disabled',true);
        $(this).find(".fa").attr("class","fa fa-spin fa-spinner");

        var form = $(this).data('form');
        var values = $(this).attr("add-values");
        var next = $(this).data("next");

        if(typeof(form) == undefined || !form){
            alert("[form] is not defined");
            return;
        }

        var formData = new FormData();
        var formSerialize = $(form).serializeArray();
           

        for(var i = 0; i < formSerialize.length; i++){
            formData.append(formSerialize[i].name,formSerialize[i].value)
        }
    
        if(values){
            values = JSON.parse(values);
            for(var key in values){
                values[key] = $(values[key]).val();
            }
            formData = $.extend(formData,values)
        }
        // var data = new FormData($(form)[0]);

        $.ajax({
            url:"",
            type:"POST",
            data:formData,
            contentType: false,
            processData: false,
            success:function(data){
                console.log(typeof(data));
                $(that).attr('disabled',false);
                $(that).find(".fa").attr("class",classes);

                if(data.denied){
                    ring("warning",data.msg)
                    return;
                }
                else if(data.succeed){
                    console.log(1);
                    if(next){
                        throughAjaxReplace(next,$(form).attr("action"));
                    }else{
                        window.location.href = link;
                    }

                    $(that).closest(".modal").modal('hide');
                    return;
                }else {
                    $(form).replaceWith($(form,xmlData(data)));
                    ring("warning","Please correct errors bellow");
                   
                }
            },
            error:function(){
                $(that).attr('disabled',false);
                $(that).find(".fa").attr("class",classes);

                ring("danger","We can't reach the server with that processs");
            }
        });
    });
}(jQuery));

/* GET MODAL WITHOUT PROMPTING */
(function($) {
    $(document).on("click",".get-modal",function(e) {        
        var that = this;
        var classes =  $(this).find(".fa").attr("class");
        $(this).attr('disabled',true);
        $(this).find(".fa").attr("class","fa fa-spin fa-spinner");

        var target = $(this).data('target');
        var next = $(this).data("next");

        if(typeof(target) == undefined || !target){
            alert("[target] is not defined");
            $(that).attr('disabled',false);
            $(that).find(".fa").attr("class",classes);
            return;
        }

        var data = $(this).data("json");
        data['csrfmiddlewaretoken'] = getCookie("csrftoken");

        // var data = new FormData($(form)[0]);
        $.ajax({
            url:"",
            type:'POST',
            data:data,
           
            success: function(data){
                $(that).attr('disabled',false);
                $(that).find(".fa").attr("class",classes);

                if(data.denied){
                    ring("warning",data.msg)
                    return;
                }

                if($(target).length <= 0){
                    $(document.body).append(data);
                }else{
                    $(target).replaceWith(data);
                }
                $(target).modal("show");
                
            },
            error:function(){
                $(that).attr('disabled',false);
                $(that).find(".fa").attr("class",classes);
            }
        });
       
    });
}(jQuery));

function showPrompt(lg_ch,text,link,color) {
    var modalLoading = '<div class="modal fade in" id="prompt-confirm">\
        <div class="modal-dialog modal-sm">\
            <div class="modal-content">\
                <div class="modal-header">\
                    <button type="button" class="close" data-dismiss="modal">&times;</button>\
                    <h4 class="modal-title text-'+color+'" style="text-align:center"><span class="glyphicon glyphicon-alert"></span> '+text+'<br>\
                    </h4>\
                </div>\
                <div class="modal-body">\
                    <div class="row" style="text-align:center">\
                        <div class="col-sm-6">\
                            <a href="'+link+'" class="btn btn-default btn-block wait"><span class="glyphicon glyphicon-ok"></span> '+lang_yes[lg_ch]+'</a>\
                        </div>\
                        <div class="col-sm-6">\
                            <button class="btn btn-default btn-block" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> '+lang_no[lg_ch]+'</button>\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>';
    $(document.body).append(modalLoading);
    $("#prompt-confirm").modal("show");
}
$(".prompt").click(function(){
    var link = $(this).data("link"),
        lg_ch = $(this).data("lg_ch"),
        text = $(this).data("text"),
        color = $(this).data("color");

    showPrompt(lg_ch,text,link,color);
});
/* Prompt fro Credentials #1 */
(function($) {
    var yes,no,title,content,lg_ch,design,
        ajax,attributes,next,bridge=true,method,forBtn;

    var overlay,modal;

    lang_yes = {
        "HT":"Wi",
        "FR":"Oui",
        "EN":"Yes",
    };
    lang_no = {
        "HT":"Non",
        "FR":"Non",
        "EN":"No",
    };

    /*$('.modal-prompt-1').on('click', function(){
        
    });*/

    $(document).on("click",".prompt-for-credentials",function(e){
        e.preventDefault();

        yes = $(this).data("yes");
        no = $(this).data("no");
        title = $(this).data("title");
        content = $(this).data("content");
        avis = $(this).data("avis");
        lg_ch = $(this).data("lg_ch");
        design = $(this).data("design");
        forBtn = $(this).attr("for-btn");
        has_form_data = $(this).attr("form-data");
        category = $(this).attr("npm");

        next = $(this).attr("next");
        ajax = $(this).attr("aria-ajax");
        json = $(this).attr("json");
        method = $(this).attr("method");
        url = $(this).attr("url");

        if(typeof(next) == 'undefined'){
            alert("[next] is not defined [Link, Object or [modal]:Modal]");
            bridge = false;
        }
        if(typeof(lg_ch) == 'undefined' || !lang_yes[lg_ch]){
            lg_ch = "EN";
        }
        if(typeof(yes) == 'undefined' || !yes){
            yes = lang_yes[lg_ch];
        }
        if(typeof(no) == 'undefined' || !no){
            no = lang_no[lg_ch];
        }
        if(typeof(title) == 'undefined' || !title){
           title = "Action confirmation";
        }
        if(typeof(content) == 'undefined' || !content){
           content = "This action required that you are aware of what you are doing";
        }
        if(typeof(avis) == 'undefined' || !avis){
            avis = "Do you still want to proceed anyway?";
        }
        if(typeof(category) == 'undefined' || !category){
           category = "easy";
        }else if (category != 'easy' && category != 'complex'){
            alert("This category doesn't exist [npm can be easy or complex]");
            bridge = false;
        }
        if(typeof(design) == 'undefined' || !design){
           design = "default gray";
        }
        if(typeof(json) == 'undefined' || !json){
           json = '';
        }
        if(typeof(method) == 'undefined' || !method){
           method = 'get';
        }
        if(typeof(url) == 'undefined' || !url){
           url = '';
        }
        if(typeof(forBtn) == 'undefined' || !forBtn){
           forBtn = 'btn-info';
        }

        

        if(ajax){
            if(next[0] != '.' && next[0] != '#' && next.slice(0,7) != '[modal]'){
                alert("[aria-ajax] is set to true, but there 's no 'class' nor 'id' nor [modal] from [next]");
                bridge = false;
            }
        }
        if(bridge)promptForCredentials();
        // alert(btnYes);
    });

    function promptForCredentials() {
        if(category == 'easy'){
            var modalLoading = '<div class="modal in" id="prompt-confirm">\
                <div class="vertical-alignment-helper">\
                <div class="modal-dialog modal-sm modal-dialog-centered" style="">\
                    <div class="modal-content">\
                        <div class="modal-header bg-'+design+'">\
                            <h5 class="modal-title" style="text-align:center">'+title+'</h5>\
                            <button type="button" class="close" data-dismiss="modal">&times;</button>\
                        </div>\
                        <div class="modal-body">\
                            <div class="row" style="text-align:center">\
                                <div class="col-sm-6">\
                                    <button id="prompt-yes" class="btn btn-sm '+forBtn+' btn-block"><span class="fa fa-check"></span> '+yes+'</button>\
                                </div>\
                                <div class="col-sm-6">\
                                    <button class="btn btn-outline-danger btn-block btn-sm" data-dismiss="modal"><span class="fa fa-remove"></span> '+no+'</button>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                </div>\
            </div>';
        }else if(category == 'complex'){
            var modalLoading = '<div class="modal-frame">\
                <div class="modalx">\
                    <div class="modal-inset">\
                        <div class="button close"><i class="fa fa-close"></i></div>\
                        <div class="modal-body">\
                            <h3>'+title+'</h3>\
                            <p>'+content+'</p>\
                            <p class="ps">'+avis.toUpperCase()+'</p>\
                        </div>\
                        <div class="modal-footer center">\
                            <button style="width: 100px" class="btn btn-danger modal-close"><i class="fa fa-remove"></i> '+no+'</button>\
                            <button id="prompt-yes" style="width: 100px" class="btn '+forBtn+'"><i class="fa fa-check"></i> '+yes+'</button>\
                        </div>\
                    </div>\
                </div>\
            </div>\
            <div class="modal-overlay"></div>';
        }   
        
        $(document.body).append(modalLoading);

        if(category == 'easy'){
            $("#prompt-confirm").modal("show");
        }else if(category == 'complex'){     

            overlay = $('.modal-overlay');
            modal = $('.modal-frame');

            $(overlay).removeClass('overlay-leave').addClass('state-show');
            $(modal).removeClass('state-leave').addClass('state-appear');

            $(document).on('click','.modal-frame .close , .modal-frame .modal-close' ,function(){
                $(overlay).removeClass('state-show').addClass('overlay-leave');
                $(modal).removeClass('state-appear').addClass('state-leave');
            });
            $(modal).bind('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e){
                // $(modal).remove();
                if($(modal).hasClass('state-leave')) {
                    $(modal).removeClass('state-leave').remove();
                }
                if($(overlay).hasClass('overlay-leave')) {
                    $(overlay).removeClass('overlay-leave').remove();
                }
            });
        }
        $(document).on('hidden.bs.modal',"#prompt-confirm",function() {
            $(this).remove();
        });
        $(document).on('shown.bs.modal',"#prompt-confirm",function() {
            $that = $(this);
        });
        $('#prompt-yes').click(function() {
            $(this).attr('disabled',true).addClass("disabled");
            $(this).find(".fa").remove();
            $(this).prepend("<i class='fa fa-spin fa-spinner'></i>");

            if(has_form_data !== undefined && has_form_data){
                var data = new FormData($(has_form_data)[0]);
            }
            else{
                var data = new FormData();
            }
            data.append("csrfmiddlewaretoken",getCookie('csrftoken'));

            var o_data = JSON.parse(json);
            for(var key in o_data){
                data.append(key, o_data[key]);
            }
            if(ajax){
                $.ajax({
                    url:url,
                    type:method,
                    data:data,
                    contentType: false,
                    processData: false,
                    success:function(data,statusCode,jqXHR){
                        if(category == 'complex'){
                            $('.modal-overlay').removeClass('state-show').addClass('overlay-leave');
                            $('.modal-frame').removeClass('state-appear').addClass('state-leave');
                        }else if(category == 'easy'){
                            $('#prompt-confirm').modal('hide');
                        }
                        if(data.denied){
                            ring("warning",data.msg)
                            return;
                        }else if(!data.url){
                            if(next.startsWith('[modal]')){
                                $(next.slice(8)).modal("show");
                                return;
                            }
                            lor = next.split(",");
                            for(var i = 0 ; i < lor.length ; i++ ){
                                if($(lor[i],$(data)).length > 0){
                                    // $(lor[i]).each(function(i,e){
                                    //    e.innerHTML = i + 1;
                                    // });
                                    $(lor[i]).replaceWith($(lor[i],$(data)));
                                }else{
                                    $(lor[i]).remove()
                                }
                            }
                        }
                        else{
                            window.location.href = data.url;
                        }
                        
                    },
                    error:function(){
                        alert("One action is pending...");
                        $('#btnYes').attr("disabled",false).css("cursor",'pointer');
                    }
                })
            }else{
                if(next)
                    window.location.href = next;
                window.location.href = "";
            }

        });
    }
}(jQuery));



(function($) {
    //open popup
    $('.cd-popup-trigger').on('click', function(event){
        event.preventDefault();
        $('.cd-popup').addClass('is-visible');
    });
    
    //close popup
    $('.cd-popup').on('click', function(event){
        if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
            event.preventDefault();
            $(this).removeClass('is-visible');
        }
    });
    //close popup when clicking the esc keyboard button
    $(document).keyup(function(event){
        if(event.which=='27'){
            $('.cd-popup').removeClass('is-visible');
        }
    });
}(jQuery));

$(".only_number").bind('keypress', function(e){
    if (e.keyCode == 65 && e.ctrlKey) {
        return true;
    }
    if(e.keyCode == 8 || e.keyCode == 9){
        return true;
    }
    if(isNumberKey(e)){
        return true;
    }else{
        return false;
    }
});
$(document).on('keypress',".only_number", function(e){
    if (e.keyCode == 65 && e.ctrlKey) {
        return true;
    }
    if(e.keyCode == 8 || e.keyCode == 9){
        return true;
    }
    if(isNumberKey(e)){
        return true;
    }else{
        return false;
    }
});



$(document).on('keypress',".only_number_with_tiret",function(e){
    if (e.keyCode == 65 && e.ctrlKey) {
        return true;
    }
    if(e.keyCode == 8 || e.keyCode == 9 || e.which == 45){
        return true;
    }
    if(isNumberKey(e)){
        return true;
    }else{
        return false;
    }
});
$(document).on('keypress',".only_number_with_dot_tiret",function(e){
    if (e.keyCode == 65 && e.ctrlKey) {
        return true;
    }
    if(e.keyCode == 8 || e.keyCode == 9 || e.which == 45 || e.which == 46){
        return true;
    }
    if(isNumberKey(e)){
        return true;
    }else{
        return false;
    }
});
$(document).on('keypress',".only_number_with_dot",function(e){
    if (e.keyCode == 65 && e.ctrlKey) {
        return true;
    }
    if(e.keyCode == 8 || e.keyCode == 9 || e.which == 46){
        return true;
    }
    if(isNumberKey(e)){
        return true;
    }else{
        return false;
    }
});
$(document).on('keypress',".only_number_with_colon",function(e){
    if (e.keyCode == 65 && e.ctrlKey) {
        return true;
    }
    if(e.keyCode == 8 || e.keyCode == 9 || e.which == 58){
        return true;
    }
    if(isNumberKey(e)){
        return true;
    }else{
        return false;
    }
});

$(".only_number_with_plus").bind('keypress', function(e){
    if (e.keyCode == 65 && e.ctrlKey) {
        return true;
    }
    if(e.keyCode == 8 || e.keyCode == 9 || e.which == 43){
        return true;
    }
    if(isNumberKey(e)){
        return true;
    }else{
        return false;
    }
});

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if((charCode < 48 || charCode > 57))
        return false;
    return true;
}


$(document).on("click",".path-next",function(){
    var url = $(this).data('href');
    if(typeof(url) !== 'undefined')
        window.location.href = url;
});
$(document).ready(function () {
    var time = 10000

    function carouselRun(time){
        // Launch the carousel
        $('.carousel').carousel({
            interval: time
        });
        $('.carousel').carousel('cycle');
    }


    carouselRun(time);


    $(document).on("mousemove",function(){
        // We clear the time
        clearTimeout(time);

        // We call it again if the mouse doesn't move
        setTimeout(function(){
            carouselRun(time);
        }, 3000);
    })


    $("#connexion").on("shown.bs.modal",function(){
        $("#input-connexion").focus();
    });
});

function isInt(str){
    if(str.indexOf(".") > -1 && str.length == 1)
        return false;
    if(str == '')
        return false;
    return /^\+?(0|[0-9]*)?(\.{0,1}|\.{0,1}\d{0,2})$/.test(str);
    // return /^\+?(0|[0-9]\d*\.?(|\d+))$/.test(str);
}

$.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
};

function scrollTo(target){
    var $target = $(target);
    if ( $target.length ) {
        $('html,body').animate({scrollTop: $target.offset().top});
    }
}

window.onload = function() {

    /*    var observe;
    if (window.attachEvent) {
        observe = function (element, event, handler) {
            element.attachEvent('on'+event, handler);
        };
    }
    else {
        observe = function (element, event, handler) {
            element.addEventListener(event, handler, false);
        };
    }*/
    

    var max_height;
    var resize  = function(t,offset) {
        var new_height = t.scrollHeight  + offset;
        if((max_height && new_height  < max_height) || (!max_height)){
            t.style.height = 'auto';
            t.style.height = new_height + 'px';  
        }  
    }
    /* 0-timeout to get the already changed text */
    function delayedResize(t,offset) {
        var that = t;
        window.setTimeout(function(){
            resize(that,offset)
        }, 0);
    }


    $(document).on("keydown , focus , cut , copy , paste , drop",".auto-resize",function(e){
        var offset = !window.opera ? (this.offsetHeight - this.clientHeight) : (this.offsetHeight + parseInt(window.getComputedStyle(this, null).getPropertyValue('border-top-width'))) ;
        max_height = $(this).attr("max-height");
        if(!isInt(max_height)){
            max_height = null;
        }

        if(e.keyCode == 8 || e.keyCode == 13) {
            var new_height = this.scrollHeight  + offset;
            if((max_height && new_height  < max_height) || (!max_height)){
                this.style.height = 'auto';
                this.style.height = (this.scrollHeight  + offset) + 'px'; 
            }
        }
        delayedResize(this,offset)
    });
    $(document).on("change",".auto-resize",function(){
        max_height = $(this).attr("max-height");
        if(!isInt(max_height)){
            max_height = null;
        }
        var offset= !window.opera ? (this.offsetHeight - this.clientHeight) : (this.offsetHeight + parseInt(window.getComputedStyle(this, null).getPropertyValue('border-top-width'))) ;
        resize(this,offset)
    });


    // observe(text, 'change',  resize);
    // observe(text, 'cut',     delayedResize);
    // observe(text, 'paste',   delayedResize);
    // observe(text, 'drop',    delayedResize);
    // observe(text, 'keydown', delayedResize);

    $(".auto-resize").each(function(i,e){
        // e.focus();
        // e.select();
        var offset = !window.opera ? (e.offsetHeight - e.clientHeight) : (e.offsetHeight + parseInt(window.getComputedStyle(e, null).getPropertyValue('border-top-width'))) ;
        resize(e,offset);
    });
}

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

$(".format-money").each(function(i,e){
    var value = $(this).text(formatMoney($(this).text()));
});