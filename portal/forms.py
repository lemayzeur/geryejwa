from django import forms
from .models import *
from django.utils.translation import ugettext_lazy as _
from geryejwa.utils import add_class_to_field

MAX_SIZE_FILE_MB = 10 # MB
MAX_SIZE_FILE_KB = MAX_SIZE_FILE_MB * 1024 # KB
MAX_SIZE_FILE = MAX_SIZE_FILE_KB * 1024 # Bytes


class PartnerForm(forms.ModelForm):
	class Meta:
		model = Partner 
		exclude = ('position','active')

	def __init__(self, *args, **kwargs):
		super(PartnerForm, self).__init__(*args, **kwargs)
		self.fields['name'].widget = forms.TextInput(attrs={
			'class': '',
			'placeholder': _('Organization name')})

		self.fields['link'].widget = forms.TextInput(attrs={
			'class': '',
			'placeholder': _('Organization name')})


		for key,field in self.fields.items():
			field.widget.attrs['class'] = 'form-control'
			field.widget.attrs['placeholder'] = field.label

	def clean(self):
		form_data = super(PartnerForm,self).clean()
		name = form_data.get("name")
		link = form_data.get("link")
		logo = form_data.get("logo")
		if logo:
			if logo.size > MAX_SIZE_FILE:
				self.add_error('file',_('file too large. 10mb max'))
		for field_name,html_rendered in self.errors.items():
			add_class_to_field(self,field_name,'is-invalid')

		return form_data