from django.db import models
from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver

# Create your models here.



class Partner(models.Model):
	date_created 					= models.DateTimeField(auto_now_add=True)
	date_modified					= models.DateTimeField(auto_now=True)

	active = models.BooleanField(default=True)
	name = models.CharField(max_length=255)
	logo = models.ImageField(upload_to='logos/')	
	link = models.CharField(max_length=2000)
	position = models.IntegerField(default=0,blank=True,null=True)

@receiver(pre_delete, sender=Partner)
def deleteFilesWithPartner(sender, instance, **kwargs):
	if instance.logo:
		try:
			instance.logo.delete()
		except:
			pass