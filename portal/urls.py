from django.conf.urls import url
from django.urls import path, re_path, include
from . import views

urlpatterns = [
    path('', views.portal,name='portal'),
    path('administrators/', views.administrators,name='xadministrators'),
    path('partners/', views.partners,name='xpartners'),
    path('partners/new', views.partnerNew,name='xpartnerNew'),
    path('partners/edit/<int:id>', views.partnerEdit,name='xpartnerEdit'),
    path('partners/delete/<int:id>', views.partnerDelete,name='xpartnerDelete'),
    path('partners/visibility/<int:id>', views.partnerVisibility,name='xpartnerVisibility'),
    path('staff/', views.staff,name='xstaff'),
    path('emails/', views.emails,name='xemails'),
]