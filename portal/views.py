from django.shortcuts import render, HttpResponseRedirect, redirect, get_object_or_404
from django.http import JsonResponse, Http404, HttpResponse
from django.urls import resolve, Resolver404, reverse
from django.utils import translation
from django.contrib import messages
from django.views.generic import TemplateView
from django.core.exceptions import PermissionDenied
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth import get_user_model

from django.contrib.auth import authenticate,login,logout
from django.utils.translation import activate, get_language_info
from django.conf import settings
from django.utils.translation import ugettext as _
from .forms import *
from .models import *

from index.models import User,Profile

import hashlib
import string
import random
# Create your views here.


def portal(request):
	if not request.user.is_authenticated:
		messages.warning(request,_("Login first to access the portal"))
		return redirect('signin')
	context = {
		'active_item':"index",
	}
	return render(request,'portal/portal.html',context)

def administrators(request):
	all_users = User.objects.filter(is_superuser=True)
	context = {'all_users':all_users}
	return render(request,'portal/administrators.html',context)

def administratorsNew(request):
	all_users = User.objects.filter(is_superuser=True)

	if request.method == 'POST' and not request.is_ajax():
		if obj.profile.code != 'A':
			data = request.POST.copy()
			data['form_is_required'] = True
			form.data = data
		
		email = request.POST.get("email")
		if not email:
			form.add_error('email','Required field')
		elif User.objects.filter(email__iexact=email).exists():
			form.add_error('email','That email is already taken')

		# if role and role.code == 'CO' and not request.POST.get("credit_category"):
		# 	form.add_error("credit_category","required field")

		if form.is_valid():
			first_name = form.cleaned_data.get("first_name")
			last_name = form.cleaned_data.get("last_name")
			email = form.cleaned_data.get("email")
			phone = form.cleaned_data.get("phone")
			area_code = form.cleaned_data.get("area_code")
			
			salt = hashlib.sha1(str(random.random()).encode()).hexdigest()[:10]
			password = salt

			user = User.objects.create_user(username=email,password=password,
				first_name=first_name,last_name=last_name,email=email)
			
			user.profile.created_by = up
			user.profile.default_password = password
			user.profile.is_password_changed = False
			user.profile.phone = phone
			user.profile.area_code = area_code
			user.profile.gender = gender
			
			userprofile.user = user
			userprofile.save()	

	context = {'all_users':all_users}
	return render(request,'index/portal-administrators-new.html',context)

def partners(request):
	all_partners = Partner.objects.all().order_by('-id')

	context = {
		'all_partners':all_partners,
	}
	return render(request,'portal/partners.html',context)

def partnerNew(request):
	form = PartnerForm(request.POST or None,request.FILES or None)
	if request.method == 'POST':
		if form.is_valid():
			form.save()
			messages.success(request,_('Partner added successfully'))
			return redirect("xpartners")
	context = {
		'form':form,
	}
	return render(request,'portal/partner-new.html',context)

def partnerEdit(request,id):
	partner = get_object_or_404(Partner,id=id)
	form = PartnerForm(request.POST or None,request.FILES or None,instance=partner)
	if request.method == 'POST':
		if form.is_valid():
			form.save()
			messages.success(request,_('Partner %(parner_name)s edited successfully') % {'parner_name':partner.name})
			return redirect("xpartners")
	context = {
		'form':form,
	}
	return render(request,'portal/partner-new.html',context)

def partnerDelete(request,id):
	partner = get_object_or_404(Partner,id=id)
	if request.method == 'POST':
		partner_name = partner.name
		partner.delete()
		messages.success(request,_('Partner %(parner_name)s removed successfully') % {'parner_name':partner_name})
		return redirect("xpartners")

	context = {
		'partner':partner,
	}
	return render(request,'portal/partner-delete.html',context)

def partnerVisibility(request,id):
	partner = get_object_or_404(Partner,id=id)
	v = request.GET.get('v')
	if request.method == 'POST':
		partner.active = not partner.active 
		partner.save()
		messages.success(request,_('Action executed successfully'))
		return redirect("xpartners")
	context = {
		'partner':partner,
		'v':v,
	}
	return render(request,'portal/partner-visibility.html',context)

def staff(request):
	context = {}
	return render(request,'portal/staff.html',context)

def emails(request):
	context = {}
	return render(request,'portal/emails.html',context)