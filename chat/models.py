from django.db import models
from django.contrib.sessions.models import Session
from index.models import Profile

# Create your models here.


class ChatCenter(models.Model):
	date_created = models.DateTimeField(auto_now_add=True)
	date_modified = models.DateTimeField(auto_now=True)

	guest_session_key = models.CharField(max_length=255) # The Anonymous user who starts the chat
	guest_email = models.EmailField()
	guest_name = models.CharField(max_length=255)

	token = models.CharField(max_length=70) # To join the chat
	joined = models.BooleanField(default=False) # Someone has joined the chat
	date_joined = models.DateTimeField(blank=True,null=True)

	# admins = models.ManyToManyField(Profile,blank=True,through='ChatAdmin') # Participants of chat center from Gerye Jwa

	ended = models.BooleanField(default=False)


class ChatAdmin(models.Model):
	date_created = models.DateTimeField(auto_now_add=True)
	date_modified = models.DateTimeField(auto_now=True)

	# user = models.ForeignKey(Profile,on_delete=models.CASCADE)
	chat_center = models.ForeignKey(ChatCenter,on_delete=models.CASCADE)

	is_active = models.DateTimeField(blank=True,null=True)
	added = models.BooleanField(default=False)
	date_added = models.DateTimeField(blank=True,null=True)
	added_by = models.PositiveIntegerField(blank=True,null=True) # ID of the admin in case he's benn added by someone else

class Chat(models.Model):
	date_created = models.DateTimeField(auto_now_add=True)
	date_modified = models.DateTimeField(auto_now=True)

	chat_center = models.ForeignKey(ChatCenter,on_delete=models.CASCADE)
	# user = models.ForeignKey(Profile,on_delete=models.SET_NULL,null=True,blank=True)
	content = models.TextField()