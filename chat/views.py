from django.shortcuts import render
from django.http import JsonResponse
from django.utils.safestring import mark_safe
import json

from django.contrib.sessions.models import Session
from .models import ChatCenter, Chat

def chat(request):
	context = {}
	if request.session:
		context['cc'] = ChatCenter.objects.filter(id=request.session.get('id_cc'),ended=False).first()
	return render(request,'chat-center.html',context)

def startChat(request):
	''' Received Info: Name, Email from the Guest''' 
	if request.method == 'POST' and request.is_ajax():
		w  = request.POST.get('w')
		if w == 'chat-info':
			name = request.POST.get("name",'').lower()
			email = request.POST.get("email",'').lower()

			if email and name:
				if not request.session.exists(request.session.session_key):
					request.session.create() # Active The Session it it was inactive
				cc = ChatCenter.objects.get_or_create(
					guest_session_key=request.session.session_key,
					ended = False,
				)[0]
				cc.guest_name = name
				cc.guest_email = email 
				cc.save()
				request.session['id_cc'] = cc.id
				
				context = {
					'cc':cc,
				}
				return render(request,'chat-center.html',context)
		elif w == 'submit-chat':
			content = request.POST.get('content')
			cc = ChatCenter.objects.filter(id=request.session.get('id_cc'),ended=False).first()
			if cc and content:
				x_chat = Chat.objects.create(chat_center=cc,content=content)
				if request.user.is_authenticated:
					x_chat.user = request.user
				context = {
					'cc':cc,
				}
				return render(request,'chat-center.html',context)
	return PermissionDenied

def room(request, room_name):
	return render(request, 'chat/room.html', {
		'room_name_json': mark_safe(json.dumps(room_name))
	})