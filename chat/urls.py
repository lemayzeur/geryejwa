from django.conf.urls import url
from django.urls import path, re_path, include
from . import views

urlpatterns = [
    path('chat/', views.chat,name='chat'),
    path('start-chat/', views.startChat,name='startChat'),
    url(r'^chat/(?P<room_name>[^/]+)/$', views.room, name='room'),
]